package com.parkjongeun;


public class Main {

    //static int pl1win = 0;
    //static int pl2win = 0;
    //static int draw = 0;

    public static void main(String[] args) {

        //new HandTest().testAll();

        //new Main().run();
        //printHumanReadableForm();

        //CardGenerator.printAllCards();

        for (int i = 0; i < 1000; ++i) {
            String[] hands = CardGenerator.getNHand(2);
            System.out.println(Hand.toString(hands[0]));
            System.out.println(Hand.toString(hands[1]));
            play(hands[0], hands[1]);
        }
        //System.out.println("P1W: " + pl1win + " P2W: " + pl2win + " Draw: " + draw);
    }

    void run() {

        play("As 8d Ad 8c 5d", "Qh Qs Jd Kd Jc");
        play("Ks Kc Jd Kd Jc", "Jh Js Jd Kd Jc");
        play("Ad Kh Ac 7h 7d", "Ah Kh Ac 7h 7d");
        play("5h 4c 3s 2s Ac", "8h 7c 6d 5s 4c");
    }

    static void play(String handStr1, String handStr2) {
        Hand hand1 = new Hand(handStr1);
        Hand hand2 = new Hand(handStr2);

        if (Hand.compare(hand1, hand2) < 0) {
            System.out.println("Player2 win");
            //pl2win++;
        } else if (Hand.compare(hand1, hand2) > 0) {
            System.out.println("Player1 win");
            //pl1win++;
        } else {
            System.out.println("Draw");
            //draw++;
        }
    }

    static void printHumanReadableForm() {
        System.out.println(Hand.toString("As 8d Ad 8c 5d"));
        System.out.println(Hand.toString("Qh Qs Jd Kd Jc"));
        System.out.println(Hand.toString("Ks Kc Jd Kd Jc"));
        System.out.println(Hand.toString("Jh Js Jd Kd Jc"));
        System.out.println(Hand.toString("Ad Kh Ac 7h 7d"));
        System.out.println(Hand.toString("Ah Kh Ac 7h 7d"));
        System.out.println(Hand.toString("5h 4c 3s 2s Ac"));
        System.out.println(Hand.toString("8h 7c 6d 5s 4c"));
    }
}


