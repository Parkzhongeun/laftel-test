package com.parkjongeun;

import java.util.Arrays;

/**
 * Created by Parkjongeun on 20/12/2016.
 */
public class Hand {


    Card[] cards;


    final static int COUNT = 5;

    public Hand(String hand) {
        String[] cards = hand.split(" ");
        if (cards.length != 5) {
            throw new IllegalArgumentException("Malformed hand string: " + hand);
        }

        this.cards = new Card[5];
        for (int i = 0; i < 5; ++i) {
            String cardString = cards[i];
            try {
                this.cards[i] = new Card(cardString);
            } catch (NullPointerException|IndexOutOfBoundsException|IllegalArgumentException e) {
                e.printStackTrace();
                throw new IllegalArgumentException("Malformed hand string: " + hand);
            }
        }
    }

    static int compare(Hand a, Hand b) {
        int[] outDetailA = new int[5];
        int[] outDetailB = new int[5];
        int rankA = a.getRankAndDetail(outDetailA);
        int rankB = b.getRankAndDetail(outDetailB);

        if (rankA != rankB) {
            if (rankA < rankB) {
                return 1;
            } else {
                return -1;
            }
        } else {
            for (int i = 0; i < 5; ++i) {
                if (outDetailA[i] < outDetailB[i]) {
                    return -1;
                } else if (outDetailA[i] > outDetailB[i]) {
                    return 1;
                }
            }
            return 0;
        }
    }


    int getRankAndDetail(int[] outDetail) {
        assertTrue(outDetail.length == 5);

        if (isRoyalFlush(cards)) {
            return 0;
        } else if (isStraightFlush(cards, outDetail)) {
            return 1;
        } else if (isFourOfAKind(cards, outDetail)) {
            return 2;
        } else if (isFullHouse(cards, outDetail)) {
            return 3;
        } else if (isFlush(cards, outDetail)) {
            return 4;
        } else if (isStraight(cards, outDetail)) {
            return 5;
        } else if (isThreeOfAKind(cards, outDetail)) {
            return 6;
        } else if (isTwoPairs(cards, outDetail)) {
            return 7;
        } else if (isOnePair(cards, outDetail)) {
            return 8;
        } else if (isHighCard(cards, outDetail)) {
            return 9;
        } else {
            throw new AssertionError();
        }
    }


    static boolean isRoyalFlush(Card[] cards) {

        if (isStraightFlush(cards, new int[1])) {

            int[] ranks = getSortedRanks(cards);
            if (ranks[0] == Rank.Ten.rank && ranks[COUNT - 1] == Rank.Ace.rank) {
                return true;
            }
        }
        return false;
    }

    static boolean isStraightFlush(Card[] cards, int[] outDetail) {
        if (isFlush(cards, new int[5])) {
            if (isStraight(cards, outDetail)) {
                return true;
            }
        }
        return false;
    }


    static boolean isFourOfAKind(Card[] cards, int[] outDetail) {
        int[] ranks = getSortedRanks(cards);

        if (ranks[0] == ranks[3]) {
            outDetail[0] = ranks[0];
            outDetail[1] = ranks[4];
            return true;
        } else if (ranks[1] == ranks[4]) {
            outDetail[0] = ranks[4];
            outDetail[1] = ranks[0];
            return true;
        } else {
            return false;
        }
    }


    static boolean isFullHouse(Card[] cards, int[] outDetail) {

        int[] ranks = getSortedRanks(cards);

        if (ranks[0] == ranks[2] && ranks[3] == ranks[4]) {
            outDetail[0] = ranks[0];
            outDetail[1] = ranks[3];
            return true;
        } else if (ranks[0] == ranks[1] && ranks[2] == ranks[4]) {
            outDetail[0] = ranks[2];
            outDetail[1] = ranks[0];
            return true;
        } else {
            return false;
        }
    }

    static boolean isFlush(Card[] cards, int[] outDetail) {
        Suit suit = cards[0].suit;
        for (int i = 1; i < 5; ++i) {
            if (cards[i].suit != suit) {
                return false;
            }
        }
        int[] ranks = getSortedRanks(cards);
        outDetail[0] = ranks[4];
        outDetail[1] = ranks[3];
        outDetail[2] = ranks[2];
        outDetail[3] = ranks[1];
        outDetail[4] = ranks[0];
        return true;
    }

    static boolean isStraight(Card[] cards, int[] outDetail) {
        int[] ranks = getSortedRanks(cards);

        if (ranks[0] == Rank.Two.rank && ranks[COUNT-1] == Rank.Ace.rank) {
            ranks[COUNT - 1] = Rank.Two.rank - 1;
            Arrays.sort(ranks);
        }

        if (isSequentialAscending(ranks)) {
            outDetail[0] = ranks[4];
            return true;
        } else {
            return false;
        }
    }

    static boolean isSequentialAscending(int[] array) {
        for (int i = 1; i < COUNT; ++i) {
            if (array[i]-1 != array[i-1]) {
                return false;
            }
        }
        return true;
    }


    static boolean isThreeOfAKind(Card[] cards, int[] outDetail) {
        int[] ranks = getSortedRanks(cards);

        if (ranks[0] == ranks[2]) {
            outDetail[0] = ranks[0];
            outDetail[1] = ranks[4];
            outDetail[2] = ranks[3];
            return true;
        } else if (ranks[1] == ranks[3]) {
            outDetail[0] = ranks[1];
            outDetail[1] = ranks[4];
            outDetail[2] = ranks[0];
            return true;
        } else if (ranks[2] == ranks[4]) {
            outDetail[0] = ranks[2];
            outDetail[1] = ranks[1];
            outDetail[2] = ranks[0];
            return true;
        } else {
            return false;
        }
    }

    static boolean isTwoPairs(Card[] cards, int[] outDetail) {
        int[] ranks = getSortedRanks(cards);


        if (ranks[0] == ranks[1] && ranks[2] == ranks[3]) {
            outDetail[0] = ranks[2];
            outDetail[1] = ranks[0];
            outDetail[2] = ranks[4];
            return true;
        } else if (ranks[0] == ranks[1] && ranks[3] == ranks[4]) {
            outDetail[0] = ranks[3];
            outDetail[1] = ranks[0];
            outDetail[2] = ranks[2];
            return true;
        } else if (ranks[1] == ranks[2] && ranks[3] == ranks[4]) {
            outDetail[0] = ranks[3];
            outDetail[1] = ranks[1];
            outDetail[2] = ranks[0];
            return true;
        } else {
            return false;
        }
    }

    static boolean isOnePair(Card[] cards, int[] outDetail) {
        int[] ranks = getSortedRanks(cards);

        if (ranks[4] == ranks[3]) {
            outDetail[0] = ranks[4];
            outDetail[1] = ranks[2];
            outDetail[2] = ranks[1];
            outDetail[3] = ranks[0];
            return true;
        } else if (ranks[3] == ranks[2]) {
            outDetail[0] = ranks[2];
            outDetail[1] = ranks[4];
            outDetail[2] = ranks[1];
            outDetail[3] = ranks[0];
            return true;
        } else if (ranks[2] == ranks[1]) {
            outDetail[0] = ranks[1];
            outDetail[1] = ranks[4];
            outDetail[2] = ranks[3];
            outDetail[3] = ranks[0];
            return true;
        } else if (ranks[1] == ranks[0]) {
            outDetail[0] = ranks[0];
            outDetail[1] = ranks[4];
            outDetail[2] = ranks[3];
            outDetail[3] = ranks[2];
            return true;
        } else {
            return false;
        }
    }

    static boolean isHighCard(Card[] cards, int[] outDetail) {
        int[] ranks = getSortedRanks(cards);
        outDetail[0] = ranks[4];
        outDetail[1] = ranks[3];
        outDetail[2] = ranks[2];
        outDetail[3] = ranks[1];
        outDetail[4] = ranks[0];
        return true;
    }

    static int[] getSortedRanks(Card[] cards) {
        int[] ranks = new int[COUNT];
        for (int i = 0; i < COUNT; ++i) {
            ranks[i] = cards[i].rank.rank;
        }
        // 오름차순 정렬
        Arrays.sort(ranks);
        return ranks;
    }

    static void assertTrue(boolean b) {
        if (!b) {
            throw new AssertionError();
        }
    }

    static String toString(String handStr) {
        final Hand hand = new Hand(handStr);
        final Card[] cards = hand.cards;
        final int[] outDetail = new int[5];

        if (isRoyalFlush(cards)) {
            return "Royal Flush";
        } else if (isStraightFlush(cards, outDetail)) {
            return "Straight Flush, " + outDetail[0];
        } else if (isFourOfAKind(cards, outDetail)) {
            return "Four of a kind, " + outDetail[0] + ", " + outDetail[1];
        } else if (isFullHouse(cards, outDetail)) {
            return "Full House, " + outDetail[0] + ", " + outDetail[1];
        } else if (isFlush(cards, outDetail)) {
            return "Flush, " + outDetail[0] + ", " + outDetail[1] + ", " + outDetail[2] + ", " + outDetail[3] + ", " + outDetail[4];
        } else if (isStraight(cards, outDetail)) {
            return "Straight, " + outDetail[0];
        } else if (isThreeOfAKind(cards, outDetail)) {
            return "Three of a kind, " + outDetail[0] + ", " + outDetail[1] + ", " + outDetail[2];
        } else if (isTwoPairs(cards, outDetail)) {
            return "Two Pair, " + outDetail[0] + ", " + outDetail[1] + ", " + outDetail[2];
        } else if (isOnePair(cards, outDetail)) {
            return "One Pair, " + outDetail[0] + ", " + outDetail[1] + ", " + outDetail[2] + ", " + outDetail[3];
        } else if (isHighCard(cards, outDetail)) {
            return "High Card, " + outDetail[0] + ", " + outDetail[1] + ", " + outDetail[2] + ", " + outDetail[3] + ", " + outDetail[4];
        } else {
            throw new AssertionError();
        }
    }
}


class Card {

    final Rank rank;
    final Suit suit;


    // IndexOutOfBoundsException
    // NullPointerException
    // IllegalArgumentException
    Card(String code) {
        char rank = code.charAt(0);
        char suit = code.charAt(1);

        this.rank = Rank.valueOf(rank);
        this.suit = Suit.valueOf(suit);
    }
}

enum Suit {
    Spades,
    Hearts,
    Diamonds,
    Clubs;

    public static Suit valueOf(char code) {
        switch (code) {
            case 's': return Spades;
            case 'h': return Hearts;
            case 'd': return Diamonds;
            case 'c': return Clubs;

            default:
                throw new IllegalArgumentException("code: " + code);
        }

    }
}

enum Rank {

    Two(2),
    Three(3),
    Four(4),
    Five(5),
    Six(6),
    Seven(7),
    Eight(8),
    Nine(9),
    Ten(10),
    Jack(11),
    Queen(12),
    King(13),
    Ace(14);

    public int rank;

    Rank(int rank) {
        this.rank = rank;
    }

    public static Rank valueOf(char code) {
        switch (code) {
            case '2': return Two;
            case '3': return Three;
            case '4': return Four;
            case '5': return Five;
            case '6': return Six;
            case '7': return Seven;
            case '8': return Eight;
            case '9': return Nine;
            case 'T': return Ten;
            case 'J': return Jack;
            case 'Q': return Queen;
            case 'K': return King;
            case 'A': return Ace;
            default:
                throw new IllegalArgumentException("code: " + code);
        }

    }
}