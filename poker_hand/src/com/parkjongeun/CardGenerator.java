package com.parkjongeun;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Parkjongeun on 21/12/2016.
 */
public class CardGenerator {


    static void printAllCards() {
        char[] number = {'2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A'};
        char[] suit = {'s', 'c', 'h', 'd'};

        for (int i = 0; i < number.length; ++i) {
            for (int j = 0; j < suit.length; ++j) {
                System.out.println(String.format("%c%c", number[i], suit[j]));
            }
        }
    }

    static String[] getDeck() {
        String[] deck = new String[52];

        char[] number = {'2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A'};
        char[] suit = {'s', 'c', 'h', 'd'};

        for (int i = 0; i < number.length; ++i) {
            for (int j = 0; j < suit.length; ++j) {
                deck[i*suit.length + j] = String.format("%c%c", number[i], suit[j]);
            }
        }
        return deck;
    }

    static String[] getDeckShuffled() {
        String[] deck = getDeck();
        shuffleArray(deck);
        return deck;
    }

    static void printStringArray(String[] strArr) {
        for (int i = 0; i < strArr.length; ++i) {
            System.out.println(strArr[i]);
        }
    }

    static String[] getNHand(int n) {
        String[] deck = getDeckShuffled();
        StringBuilder[] hands = new StringBuilder[n];
        for (int i = 0; i < n; ++i) {
            hands[i] = new StringBuilder();
        }
        int k = 0;
        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < hands.length; ++j) {
                if (i > 0) {
                    hands[j].append(' ');
                }
                hands[j].append(deck[k++]);
            }
        }
        String[] handStrs = new String[n];
        for (int i = 0; i < n; ++i) {
            handStrs[i] = hands[i].toString();
        }
        return handStrs;
    }


    // http://stackoverflow.com/questions/1519736/random-shuffling-of-an-array
    // Implementing Fisher–Yates shuffle
    static void shuffleArray(String[] ar)
    {
        // If running on Java 6 or older, use `new Random()` on RHS here
        Random rnd = ThreadLocalRandom.current();
        for (int i = ar.length - 1; i > 0; i--)
        {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            String a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
    }
}
