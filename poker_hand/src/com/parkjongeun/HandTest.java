package com.parkjongeun;

/**
 * Created by Parkjongeun on 21/12/2016.
 */
public class HandTest {

    void testAll() {
        testGetSortedRank();
        testIsSequential();


        testIsStraight();
        testIsTwoPair();
        testIsThreeOfAKind();
        testIsFlush();
        testIsFullHouse();
        testFourOfAKind();
        testIsStraightFlush();
        testRoyalFlush();
        testIsOnePair();
        testHighCard();
    }

    void testIsStraight() {
        int[] outDetail;

        outDetail = new int[5];
        assertTrue(Hand.isStraight(new Hand("5c 4d 3d 2c Ah").cards, outDetail));
        assertTrue(outDetail[0] == Rank.Five.rank);

        outDetail = new int[5];
        assertTrue(Hand.isStraight(new Hand("6c 5d 4d 3c 2h").cards, outDetail));
        assertTrue(outDetail[0] == Rank.Six.rank);

        outDetail = new int[5];
        assertTrue(Hand.isStraight(new Hand("Ac Kd Qd Jc Th").cards, outDetail));
        assertTrue(outDetail[0] == Rank.Ace.rank);

        outDetail = new int[5];
        assertFalse(Hand.isStraight(new Hand("Ac Kd Qd Jc 9h").cards, outDetail));
        assertTrue(outDetail[0] == 0);

        outDetail = new int[5];
        assertFalse(Hand.isStraight(new Hand("Ac Jd Qd Jc 9h").cards, outDetail));
        assertTrue(outDetail[0] == 0);

        outDetail = new int[5];
        assertFalse(Hand.isStraight(new Hand("Ac Jc Qc Jc 9c").cards, outDetail));
        assertTrue(outDetail[0] == 0);
    }

    void testIsTwoPair() {
        int[] outDetail = new int[5];
        assertTrue(Hand.isTwoPairs(new Hand("Jc 3c 3c 8c 8c").cards, outDetail));
        assertEqual(outDetail[0], 8);
        assertEqual(outDetail[1], 3);

        assertTrue(Hand.isTwoPairs(new Hand("Jc Jc 3c 8c 8c").cards, outDetail));
        assertEqual(outDetail[0], Rank.Jack.rank);
        assertEqual(outDetail[1], Rank.Eight.rank);

        assertTrue(Hand.isTwoPairs(new Hand("Jc Jc 8c 8c 8c").cards, outDetail));
        assertEqual(outDetail[0], Rank.Jack.rank);
        assertEqual(outDetail[1], Rank.Eight.rank);

        outDetail = new int[5];
        assertFalse(Hand.isTwoPairs(new Hand("Jc Jc Tc 5c 8c").cards, outDetail));
        assertEqual(outDetail[0], 0);

        outDetail = new int[5];
        assertFalse(Hand.isTwoPairs(new Hand("5c 6c 7c 8c 9c").cards, outDetail));
        assertEqual(outDetail[0], 0);
    }

    void testIsThreeOfAKind() {

        int[] outDetail = new int[5];
        assertTrue(Hand.isThreeOfAKind(new Hand("Jc 3c 3c 8c 3c").cards, outDetail));
        assertEqual(outDetail[0], 3);
        assertEqual(outDetail[1], 11);
        assertEqual(outDetail[2], 8);

        assertTrue(Hand.isThreeOfAKind(new Hand("Jc 3c 3c 3c 3c").cards, outDetail));
        assertEqual(outDetail[0], 3);
        assertEqual(outDetail[1], 11);
        assertEqual(outDetail[2], 3);

        outDetail = new int[5];
        assertFalse(Hand.isThreeOfAKind(new Hand("Jc Jc 3c 3c Ac").cards, outDetail));
        assertEqual(outDetail[0], 0);
    }

    void testIsFlush() {
        int[] outDetail = new int[5];
        assertTrue(Hand.isFlush(new Hand("Jh 3h 3h 8h 3h").cards, outDetail));
        assertEqual(outDetail[0], 11);
        assertEqual(outDetail[1], 8);
        assertEqual(outDetail[2], 3);
        assertEqual(outDetail[3], 3);
        assertEqual(outDetail[4], 3);

        assertTrue(Hand.isFlush(new Hand("As 3s Ts Ks Qs").cards, outDetail));
        assertEqual(outDetail[0], 14);
        assertEqual(outDetail[1], 13);
        assertEqual(outDetail[2], 12);
        assertEqual(outDetail[3], 10);
        assertEqual(outDetail[4], 3);

        outDetail = new int[5];
        assertFalse(Hand.isFlush(new Hand("Jh 3h 3h 8c 3h").cards, outDetail));
        assertEqual(outDetail[0], 0);
    }

    void testIsFullHouse() {
        int[] outDetail = new int[5];
        assertTrue(Hand.isFullHouse(new Hand("Jh 3c 3h Jh 3h").cards, outDetail));
        assertEqual(outDetail[0], 3);
        assertEqual(outDetail[1], 11);

        assertTrue(Hand.isFullHouse(new Hand("Ah Ac 3h Ah 3h").cards, outDetail));
        assertEqual(outDetail[0], 14);
        assertEqual(outDetail[1], 3);

        assertFalse(Hand.isFullHouse(new Hand("Ah Ac 3h 2h 3h").cards, outDetail));
    }

    void testFourOfAKind() {
        int[] outDetail = new int[5];
        assertTrue(Hand.isFourOfAKind(new Hand("Jh 3c Jh Jh Jh").cards, outDetail));
        assertEqual(outDetail[0], 11);
        assertEqual(outDetail[1], 3);

        assertTrue(Hand.isFourOfAKind(new Hand("Jh Ac Ah Ah Ah").cards, outDetail));
        assertEqual(outDetail[0], 14);
        assertEqual(outDetail[1], 11);

        assertFalse(Hand.isFourOfAKind(new Hand("Jh Ac Ah Ah 2h").cards, outDetail));
    }

    void testIsStraightFlush() {
        int[] outDetail = new int[5];
        assertTrue(Hand.isStraightFlush(new Hand("Jh 7h 9h 8h Th").cards, outDetail));
        assertEqual(outDetail[0], 11);

        assertTrue(Hand.isStraightFlush(new Hand("4h 5h 2h 3h Ah").cards, outDetail));
        assertEqual(outDetail[0], 5);

        assertTrue(Hand.isStraightFlush(new Hand("Ah Kh Qh Jh Th").cards, outDetail));
        assertEqual(outDetail[0], 14);

        assertFalse(Hand.isStraightFlush(new Hand("Ah Kh Qc Jh Th").cards, outDetail));
    }

    void testRoyalFlush() {
        assertTrue(Hand.isRoyalFlush(new Hand("Ah Kh Qh Jh Th").cards));

        assertFalse(Hand.isRoyalFlush(new Hand("4h 5h 2h 3h Ah").cards));

        assertFalse(Hand.isRoyalFlush(new Hand("4h 5h 2h 3c Ah").cards));

        assertFalse(Hand.isRoyalFlush(new Hand("Jh 7h 9h 8h Th").cards));

        assertFalse(Hand.isRoyalFlush(new Hand("Ah Ks Qh Jh Th").cards));
    }

    void testIsOnePair() {

        int[] outDetail = new int[5];
        assertTrue(Hand.isOnePair(new Hand("2h 7h 9h 8h 9h").cards, outDetail));
        assertEqual(outDetail[0], 9);
        assertEqual(outDetail[1], 8);
        assertEqual(outDetail[2], 7);
        assertEqual(outDetail[3], 2);

        assertTrue(Hand.isOnePair(new Hand("5h 7h Jh 5h Jh").cards, outDetail));
        assertEqual(outDetail[0], 11);
        assertEqual(outDetail[1], 7);
        assertEqual(outDetail[2], 5);
        assertEqual(outDetail[3], 5);

        assertTrue(Hand.isOnePair(new Hand("5h Ah Jh Ah Jh").cards, outDetail));
        assertEqual(outDetail[0], 14);
        assertEqual(outDetail[1], 11);
        assertEqual(outDetail[2], 11);
        assertEqual(outDetail[3], 5);

        assertTrue(Hand.isOnePair(new Hand("5h Ah Ah Ah Jh").cards, outDetail));
        assertEqual(outDetail[0], 14);
        assertEqual(outDetail[1], 14);
        assertEqual(outDetail[2], 11);
        assertEqual(outDetail[3], 5);

        assertTrue(Hand.isOnePair(new Hand("5h 2h Ah Ah Jh").cards, outDetail));
        assertEqual(outDetail[0], 14);
        assertEqual(outDetail[1], 11);
        assertEqual(outDetail[2], 5);
        assertEqual(outDetail[3], 2);

        assertTrue(Hand.isOnePair(new Hand("2h 4h 7h Ah 2h").cards, outDetail));
        assertEqual(outDetail[0], 2);
        assertEqual(outDetail[1], 14);
        assertEqual(outDetail[2], 7);
        assertEqual(outDetail[3], 4);

        assertTrue(Hand.isOnePair(new Hand("2h Th 7h Th Ah").cards, outDetail));
        assertEqual(outDetail[0], 10);
        assertEqual(outDetail[1], 14);
        assertEqual(outDetail[2], 7);
        assertEqual(outDetail[3], 2);
    }

    void testHighCard() {
        int[] outDetail = new int[5];
        assertTrue(Hand.isHighCard(new Hand("2h 7h 9h 8h 9h").cards, outDetail));
        assertEqual(outDetail[0], 9);
        assertEqual(outDetail[1], 9);
        assertEqual(outDetail[2], 8);
        assertEqual(outDetail[3], 7);
        assertEqual(outDetail[4], 2);

        assertTrue(Hand.isHighCard(new Hand("2h 7h 9h Ah 9h").cards, outDetail));
        assertEqual(outDetail[0], 14);
        assertEqual(outDetail[1], 9);
        assertEqual(outDetail[2], 9);
        assertEqual(outDetail[3], 7);
        assertEqual(outDetail[4], 2);

        assertTrue(Hand.isHighCard(new Hand("2h 2h 2h 2h 9h").cards, outDetail));
        assertEqual(outDetail[0], 9);
        assertEqual(outDetail[1], 2);
        assertEqual(outDetail[2], 2);
        assertEqual(outDetail[3], 2);
        assertEqual(outDetail[4], 2);
    }

    void testGetSortedRank() {
        assertTrue(Hand.getSortedRanks(new Hand("6c 5d 4d 3c 2h").cards)[4] == 6);
        assertTrue(Hand.getSortedRanks(new Hand("6c 5d 4d 3c 2h").cards)[3] == 5);
        assertTrue(Hand.getSortedRanks(new Hand("6c 5d 4d 3c 2h").cards)[2] == 4);
        assertTrue(Hand.getSortedRanks(new Hand("6c 5d 4d 3c 2h").cards)[1] == 3);
        assertTrue(Hand.getSortedRanks(new Hand("6c 5d 4d 3c 2h").cards)[0] == 2);
    }

    void testIsSequential() {
        assertFalse(Hand.isSequentialAscending(new int[]{5, 4, 3, 2, 1}));
        assertTrue(Hand.isSequentialAscending(new int[]{1, 2, 3, 4, 5}));
    }

    static void assertTrue(boolean b) {
        if (!b) {
            throw new AssertionError();
        }
    }

    static void assertFalse(boolean b) {
        if (b) {
            throw new AssertionError();
        }
    }

    static void assertEqual(int expected, int actual) {
        if (expected != actual) {
            throw new AssertionError("expected: " + expected + " actual: " + actual);
        }
    }
}
