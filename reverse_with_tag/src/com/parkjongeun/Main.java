package com.parkjongeun;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.NodeVisitor;

import java.util.Stack;

public class Main {

    /**
     * HTML 태그가 포함된 문자열에서 태그를 모두 제거 하였을 때 남아 있는 전체 문자열을 뒤집는 문제입니다.
     *
     * 알고리즘:
     * 먼저, HTML 태그가 포함된 문자열을 HTML 파서를 이용하여 DOM 트리를 만듭니다.
     * (이때, 텍스트 노드들은 모두 리프 노드 입니다.)
     * 이 트리를 깊이-우선으로 순회하며 리프 노드들의 텍스트를 스택에 쌓아 둡니다.
     * 그리고 다시 한번 깊이-우선으로 순회하며 리프 노드들을 방문할 때 마다
     * 그 스택에서 텍스트를 하나씩 꺼내어 뒤집은 다음 이 리프 노드의 텍스트를 이로 바꿉니다.
     *
     */

    public static void main(String[] args) {

        String html = "The quick <font color=\"brown\">brown</font> fox jumps over the lazy dog";

        System.out.println(reverseWithTag(html));
    }

    static String reverseWithTag(String html) {

        // 이 HTML 문자열의 DOM 트리를 만듭니다.
        Document doc = Jsoup.parse(html);

        Stack<String> stack = new Stack<>();

        // 깊이-우선 순회하며 각 텍스트노드의 텍스트들을 스택에 푸시합니다.
        doc.traverse(new NodeVisitor() {
            @Override
            public void head(Node node, int depth) {
                if (node instanceof TextNode) {
                    TextNode textNode = (TextNode) node;
                    String text = textNode.getWholeText();
                    stack.push(text);
                }
            }

            @Override
            public void tail(Node node, int depth) {
                // Do nothing.
            }
        });

        // 다시 깊이-우선 순회하며 텍스트노드를 방문할 때마다 스택에서 텍스트를 하나씩 팝합니다.
        // 그리고 이 텍스트를 뒤집은 다음 이 텍스트노드의 텍스트로 셋합니다.
        doc.traverse(new NodeVisitor() {
            @Override
            public void head(Node node, int depth) {
                if (node instanceof TextNode) {
                    TextNode textNode = (TextNode) node;

                    String text = stack.pop();
                    String reversed = new StringBuilder(text).reverse().toString();
                    textNode.text(reversed);
                }
            }

            @Override
            public void tail(Node node, int depth) {
                // Do nothing.
            }
        });

        return doc.toString();
    }
}
