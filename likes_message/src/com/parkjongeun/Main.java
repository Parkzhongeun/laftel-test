package com.parkjongeun;

public class Main {

    // {no one} {likes} this.
    // {Rafy} {likes} this.
    // {Rafy, Ryan and Curian} {like} this.
    // {Rafy, Ryan and 2 others} {like} this.
    // {Rafy, Ryan and 3 others} {like} this.

    // variable 부분
    // likes | like
    // no one |

    // 문자열 배열을 받아서 문법적으로 올바른 문장으로 번역한다.
    // 동사 like의 경우, 배열의 요소 갯수에 따라 결정 된다고 가정한다.
    // 이름 리스트의 경우 배열 요소의 갯수에 따라 적절히 번역 해야 한다.

    // Test driven development

    // 참고:
    // https://docs.oracle.com/javase/tutorial/i18n/format/choiceFormat.html
    // https://developer.android.com/guide/topics/resources/string-resource.html

    public static void main(String[] args) {
        test();


        System.out.println(likeThis(new String[0]));
        System.out.println(likeThis(new String[]{"Rafy"}));
        System.out.println(likeThis(new String[]{"Rafy", "Ryan"}));
        System.out.println(likeThis(new String[]{"Rafy", "Ryan", "Curian"}));
        System.out.println(likeThis(new String[]{"Rafy", "Ryan", "Curian", "Billy"}));
        System.out.println(likeThis(new String[]{"Rafy", "Ryan", "Curian", "Billy", "Green"}));
    }

    static void test() {
        testCompressNameList();
        testLikeThis();
    }

    static String likeThis(String[] names) {
        int count = names.length;
        // 문자열 템플릿을 선택합니다.
        String template = choiceFormat(count);

        // 이름 리스트를 축약합니다.
        String compressedNameList = compressNameList(names);

        // 전체 문장을 만듭니다.
        return String.format(template, compressedNameList);
    }

    // count에 따라 문자열 템플릿을 선택하여 반환합니다.
    static String choiceFormat(int count) {
        String[] formats = {
                "%s likes this.",
                "%s likes this.",
                "%s like this."
        };
        if (count == 0) {
            return formats[0];
        } else if (count == 1) {
            return formats[1];
        } else {
            return formats[2];
        }
    }

    // 이름 리스트의 축약된 이름 리스트를 반환합니다.
    static String compressNameList(String[] names) {
        if (names == null) {
            throw new IllegalArgumentException("names.length is 0.");
        }
        if (names.length == 0) {
            return "No one";
        } else if (names.length == 1) {
            return names[0];
        } else if (names.length == 2) {
            return String.format("%s and %s", names[0], names[1]);
        } else if (names.length == 3) {
            return String.format("%s, %s and %s", names[0], names[1], names[2]);
        } else {
            return String.format("%s, %s and %s others", names[0], names[1], names.length - 2);
        }
    }

    static void testCompressNameList() {
        assertEquals("No one", compressNameList(new String[0]));

        assertEquals("Rafy", compressNameList(new String[]{"Rafy"}));

        assertEquals("Rafy and Ryan", compressNameList(new String[]{"Rafy", "Ryan"}));

        assertEquals("Rafy, Ryan and Curian", compressNameList(new String[]{"Rafy", "Ryan", "Curian"}));

        assertEquals("Rafy, Ryan and 2 others", compressNameList(new String[]{"Rafy", "Ryan", "Curian", "Billy"}));

        assertEquals("Rafy, Ryan and 3 others", compressNameList(new String[]{"Rafy", "Ryan", "Curian", "Billy", "Green"}));
    }

    static void testLikeThis() {
        assertEquals("No one likes this.",
                likeThis(new String[0]));

        assertEquals("Rafy likes this.",
                likeThis(new String[]{"Rafy"}));

        assertEquals("Rafy and Ryan like this.",
                likeThis(new String[]{"Rafy", "Ryan"}));

        assertEquals("Rafy, Ryan and Curian like this.",
                likeThis(new String[]{"Rafy", "Ryan", "Curian"}));

        assertEquals("Rafy, Ryan and 2 others like this.",
                likeThis(new String[]{"Rafy", "Ryan", "Curian", "Billy"}));

        assertEquals("Rafy, Ryan and 3 others like this.",
                likeThis(new String[]{"Rafy", "Ryan", "Curian", "Billy", "Green"}));
    }

    static void assertEquals(String expected, String actual) {
        if (!expected.equals(actual)) {
            throw new AssertionError("expected: " + expected + " actual: " + actual);
        }
    }

}
